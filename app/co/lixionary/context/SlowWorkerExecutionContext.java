package co.lixionary.context;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import play.libs.concurrent.CustomExecutionContext;

public class SlowWorkerExecutionContext extends CustomExecutionContext {

    @Inject
    public SlowWorkerExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, "slowWorker.dispatcher");
    }
}
