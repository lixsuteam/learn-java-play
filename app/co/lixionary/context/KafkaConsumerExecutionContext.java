package co.lixionary.context;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import play.libs.concurrent.CustomExecutionContext;

public class KafkaConsumerExecutionContext extends CustomExecutionContext {

    @Inject
    public KafkaConsumerExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, "kafka-consumer.dispatcher");
    }
}
