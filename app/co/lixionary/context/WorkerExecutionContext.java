package co.lixionary.context;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import play.libs.concurrent.CustomExecutionContext;

public class WorkerExecutionContext extends CustomExecutionContext {

    @Inject
    public WorkerExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, "worker.dispatcher");
    }
}
