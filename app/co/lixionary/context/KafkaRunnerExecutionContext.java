package co.lixionary.context;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import play.libs.concurrent.CustomExecutionContext;

public class KafkaRunnerExecutionContext extends CustomExecutionContext {

    @Inject
    public KafkaRunnerExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, "kafka-runner.dispatcher");
    }
}
