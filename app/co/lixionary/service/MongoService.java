package co.lixionary.service;

import co.lixionary.client.NvMongoClient;
import com.google.inject.Inject;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MongoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoService.class.getName());

    private final NvMongoClient nvMongoClient;

    @Inject
    public MongoService(NvMongoClient nvMongoClient) {
        this.nvMongoClient = nvMongoClient;
    }

    private boolean createCollection(String collectionName) {
        try {
            nvMongoClient.getMongoDatabase().createCollection(collectionName);
            return true;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return false;
        }
    }

    public MongoCollection<Document> getCollection(String collectionName) {
        try {
            return nvMongoClient.getMongoDatabase().getCollection(collectionName);
        } catch (IllegalArgumentException ex) {
            boolean isCreated = createCollection(collectionName);
            if (isCreated) {
                return nvMongoClient.getMongoDatabase().getCollection(collectionName);
            } else {
                throw new RuntimeException("collection name invalid");
            }
        }
    }
}
