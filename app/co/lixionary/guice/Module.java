package co.lixionary.guice;

import co.lixionary.client.NvMongoClient;
import co.lixionary.manager.KafkaManager;
import co.lixionary.manager.factory.PersonManagerFactory;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;

public class Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(NvMongoClient.class).asEagerSingleton();
        bind(KafkaManager.class).asEagerSingleton();

        install(new FactoryModuleBuilder().build(PersonManagerFactory.class));
    }
}
