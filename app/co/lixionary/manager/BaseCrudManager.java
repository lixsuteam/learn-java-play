package co.lixionary.manager;

import co.lixionary.dao.BaseDao;

public abstract class BaseCrudManager<T, ID, D extends BaseDao<T, ID>> {

    private final D dao;

    public BaseCrudManager(D dao) {
        this.dao = dao;
    }

    public D getDao() {
        return dao;
    }
}
