package co.lixionary.manager;

import co.lixionary.model.Person;
import com.google.inject.ImplementedBy;

import java.util.List;
import java.util.concurrent.CompletionStage;

@ImplementedBy(IMysqlPersonManager.class)
public interface PersonManager {

    CompletionStage<Person> create(Person person);

    CompletionStage<List<Person>> getAll();

    CompletionStage<Person> get(String name);
}
