package co.lixionary.manager;

import co.lixionary.context.WorkerExecutionContext;
import co.lixionary.exception.NvNotFoundException;
import co.lixionary.model.Person;
import co.lixionary.service.MongoService;
import co.lixionary.util.JsonUtil;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static com.mongodb.client.model.Filters.eq;

public class IMongoPersonManager implements PersonManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(IMongoPersonManager.class.getName());

    private final String mongoCollectionName = "people";

    private final WorkerExecutionContext executionContext;
    private final MongoService mongoService;
    private final String requestId;
    private final String systemId;

    @Inject
    public IMongoPersonManager(MongoService mongoService, WorkerExecutionContext executionContext,
                               @Assisted("requestId") String requestId,
                               @Assisted("systemId") String systemId) {
        this.executionContext = executionContext;
        this.mongoService = mongoService;
        this.requestId = requestId;
        this.systemId = systemId;
        LOGGER.info("IMongoPersonManager created");
    }

    @Override
    public CompletionStage<Person> create(Person person) {
        return CompletableFuture.supplyAsync(() -> {
            person.setSystemId(systemId);

            MongoCollection<Document> document = mongoService.getCollection(mongoCollectionName);
            document.insertOne(Document.parse(JsonUtil.toJsonSnakeCase(person)));
            return findByName(person.getName());
        }, executionContext);

    }

    @Override
    public CompletionStage<List<Person>> getAll() {
        return CompletableFuture.supplyAsync(() -> {
            MongoCollection<Document> document = mongoService.getCollection(mongoCollectionName);
            return document
                    .find()
                    .map(doc -> JsonUtil.fromJson(doc.toJson(), Person.class))
                    .into(new ArrayList<>());
        }, executionContext);
    }

    @Override
    public CompletionStage<Person> get(String name) {
        return CompletableFuture.supplyAsync(() -> findByName(name), executionContext);
    }

    private Person findByName(String name) {
        MongoCollection<Document> document = mongoService.getCollection(mongoCollectionName);
        Bson filter = eq("name", name);
        List<Person> results = document
                .find(filter)
                .map(doc -> JsonUtil.fromJson(doc.toJson(), Person.class))
                .into(new ArrayList<>());
        if (results.isEmpty()) {
            throw new NvNotFoundException(String.format("can not find person: %s", name));
        } else if (results.size() == 1) {
            return results.get(0);
        } else {
            LOGGER.warn("results more than 1");
            return results.get(0);
        }
    }
}
