package co.lixionary.manager;

import co.lixionary.dao.PersonDao;
import co.lixionary.model.Person;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class IMysqlPersonManager extends BaseCrudManager<Person, Integer, PersonDao> implements PersonManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(IMysqlPersonManager.class.getName());

    private final String requestId;
    private final String systemId;

    @Inject
    public IMysqlPersonManager(PersonDao personDao,
                               @Assisted("requestId") String requestId,
                               @Assisted("systemId") String systemId) {
        super(personDao);
        this.requestId = requestId;
        this.systemId = systemId;
        LOGGER.info("re-instantiate person manager, ref: " + this.toString());
    }

    @Override
    public CompletionStage<Person> create(Person person) {
        LOGGER.info("{} | creating person: {}", requestId, person.getName());
        person.setSystemId(systemId);
        return getDao().create(person);
    }

    @Override
    public CompletionStage<List<Person>> getAll() {
        LOGGER.info("{} | read all people", requestId);
        return getDao().read().thenApply(stream -> stream.collect(Collectors.toList()));
    }

    @Override
    public CompletionStage<Person> get(String name) {
        LOGGER.info("{} | read person with name: {}", requestId, name);
        return getDao().findByName(name);
    }
}
