package co.lixionary.manager;

import co.lixionary.context.KafkaConsumerExecutionContext;
import co.lixionary.context.KafkaRunnerExecutionContext;
import co.lixionary.kafka.ConsumerCreator;
import co.lixionary.kafka.GenericConsumer;
import co.lixionary.kafka.ProducerCreator;
import com.google.inject.Inject;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;

public class KafkaManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaManager.class.getName());

    private final Consumer<Long, String> kafkaConsumer;
    private final Producer<Long, String> kafkaProducer;
    private final KafkaRunnerExecutionContext runnerExecutionContext;
    private final KafkaConsumerExecutionContext executionContext;

    @Inject
    public KafkaManager(KafkaConsumerExecutionContext executionContext, KafkaRunnerExecutionContext runnerExecutionContext) {
        this.kafkaConsumer = ConsumerCreator.createConsumer();
        this.kafkaProducer = ProducerCreator.createProducer();
        this.executionContext = executionContext;
        this.runnerExecutionContext = runnerExecutionContext;

        init();
    }

    private void init() {
        LOGGER.info("init block @{}", Thread.currentThread().getId());
        CompletableFuture.runAsync(this::startConsumer, runnerExecutionContext);
    }

    private void startConsumer() {
        LOGGER.info("Consumer started @{}, available topics: {}",
                Thread.currentThread().getId(),
                String.join(", ", kafkaConsumer.listTopics().keySet()));
        while (true) {
            ConsumerRecords<Long, String> consumerRecords = kafkaConsumer.poll(Duration.ofMillis(5000));
            if (consumerRecords.count() != 0) {
                LOGGER.info("consuming..... {} message", consumerRecords.count());
                consumerRecords.forEach(record -> CompletableFuture.runAsync(() -> new GenericConsumer<>(record).run(), executionContext));
                // commits the offset of record to broker.
                kafkaConsumer.commitSync();
            }
        }
    }

    public Producer<Long, String> getProducer() {
        return kafkaProducer;
    }
}
