package co.lixionary.manager.factory;

import co.lixionary.manager.IMongoPersonManager;
import co.lixionary.manager.IMysqlPersonManager;
import com.google.inject.assistedinject.Assisted;

public interface PersonManagerFactory {

    IMysqlPersonManager getPersonManager(@Assisted("requestId") String requestId, @Assisted("systemId") String systemId);

    IMongoPersonManager getMongoPersonManager(@Assisted("requestId") String requestId, @Assisted("systemId") String systemId);

}
