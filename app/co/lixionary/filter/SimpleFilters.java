package co.lixionary.filter;

import com.google.inject.Inject;
import play.api.http.EnabledFilters;
import play.filters.cors.CORSFilter;
import play.http.DefaultHttpFilters;
import play.mvc.EssentialFilter;

import java.util.ArrayList;
import java.util.List;

public class SimpleFilters extends DefaultHttpFilters {

    @Inject
    public SimpleFilters(EnabledFilters enabledFilters, CORSFilter corsFilter) {
        super(combine(enabledFilters.asJava().getFilters(), corsFilter.asJava()));
    }

    private static List<EssentialFilter> combine(
            List<EssentialFilter> filters, EssentialFilter toAppend) {
        List<EssentialFilter> combinedFilters = new ArrayList<>(filters);
        combinedFilters.add(toAppend);
        return combinedFilters;
    }
}
