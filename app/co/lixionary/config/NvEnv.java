package co.lixionary.config;

import play.Logger;

public enum NvEnv {

    NV_MONGO_DB_USERNAME("NV_MONGO_DB_USERNAME", "user"),
    NV_MONGO_DB_PASSWORD("NV_MONGO_DB_PASSWORD", "password"),
    NV_MONGO_DB_HOST("NV_MONGO_DB_HOST", "localhost"),
    NV_MONGO_DB_PORT("NV_MONGO_DB_PORT", "27017"),
    NV_MONGO_DB_NAME("NV_MONGO_DB_NAME", "learn-java-play");

    private String key;
    private String value;

    NvEnv(String key, String defaultValue) {
        Logger.ALogger log = Logger.of(NvEnv.class);
        this.key = key;

        String envVal = System.getenv(key);
        if (envVal != null) {
            log.info("Key(" + key + ")  => " + envVal);
            this.value = envVal;
        } else {
            log.warn("!!! Key(" + key + ") => " + defaultValue + " (Default)");
            this.value = defaultValue;
        }
    }

    public String getValue() {
        return value;
    }
}
