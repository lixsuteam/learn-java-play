package co.lixionary.config;

import co.lixionary.util.OtherUtil;

public interface KafkaConstants {
    String KAFKA_BROKERS = "192.168.0.7:9092";
    //    String KAFKA_BROKERS = "10.255.4.227:9092,10.255.4.227:9093";
    Integer MESSAGE_COUNT = 1000;
    String CLIENT_ID = "client1";
    String GROUP_ID_CONFIG = OtherUtil.getHostName();
    Integer MAX_NO_MESSAGE_FOUND_COUNT = 100;
    String OFFSET_RESET_LATEST = "latest";
    String OFFSET_RESET_EARLIER = "earliest";
    Integer MAX_POLL_RECORDS = 20;

    String TOPIC_DEMO_1 = "demo-1";
    String TOPIC_DEMO_2 = "demo-2";
}