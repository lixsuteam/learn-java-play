package co.lixionary.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;

public class OtherUtil {

    public static final String HOST_NAME;
    private static final Logger LOGGER = LoggerFactory.getLogger(OtherUtil.class.getName());

    static {
        String hostName = "default-host";
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        } finally {
            HOST_NAME = hostName;
        }
    }

    public static String getHostName() {
        return HOST_NAME;
    }
}
