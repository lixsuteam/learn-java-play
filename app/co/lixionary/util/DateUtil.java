package co.lixionary.util;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created on 19/07/18.
 *
 * @author Felix Soewito
 */
@SuppressWarnings("WeakerAccess")
public class DateUtil {
    private final static String DATE_TIME_FORMAT = "yyy-MM-dd HH:mm:ss";
    public final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
    private final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    private final static String TIME_FORMAT_1 = "HH:mm:ss";
    public final static DateTimeFormatter TIME_FORMATTER_1 = DateTimeFormatter.ofPattern(TIME_FORMAT_1);
    private final static String TIME_FORMAT_2 = "HH:mm";
    public final static DateTimeFormatter TIME_FORMATTER_2 = DateTimeFormatter.ofPattern(TIME_FORMAT_2);
    private final static String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public final static DateTimeFormatter ISO8601_FORMATTER = DateTimeFormatter.ofPattern(ISO8601_FORMAT);
    private final static String ISO8601_FORMAT_LITE = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public final static DateTimeFormatter ISO8601_LITE_FORMATTER = DateTimeFormatter.ofPattern(ISO8601_FORMAT_LITE);
    private static ZoneId DEFAULT_ZONE_ID = ZoneId.of("Asia/Jakarta");

    public static ZonedDateTime getDate() {
        return getDate(DEFAULT_ZONE_ID);
    }

    public static ZonedDateTime getDate(ZoneId zoneId) {
        return ZonedDateTime.now(zoneId);
    }

    public static ZonedDateTime getDate(Instant instant) {
        return getDate(instant, DEFAULT_ZONE_ID);
    }

    public static ZonedDateTime getDate(Instant instant, ZoneId zoneId) {
        return ZonedDateTime.ofInstant(instant, zoneId);
    }

    public static ZonedDateTime getDate(String str) {
        return ZonedDateTime.parse(str);
    }

    public static ZonedDateTime getDate(String str, DateTimeFormatter dtf) {
        return ZonedDateTime.parse(str, dtf);
    }

    public static String displayDateTime(ZonedDateTime zdt) {
        return DATE_TIME_FORMATTER.format(zdt);
    }

    public static String displayDateTime(Instant instant, ZoneId zoneId) {
        return displayDateTime(getDate(instant, zoneId));
    }

    public static String displayDate(ZonedDateTime zdt) {
        return DATE_FORMATTER.format(zdt);
    }

    public static String displayDate(Instant instant, ZoneId zoneId) {
        return displayDate(getDate(instant, zoneId));
    }

    /**
     * Return date String in yyy-MM-dd HH:mm:ss.
     *
     * @param zdt Object ZonedDateTime.
     * @return date String in yyy-MM-dd HH:mm:ss.
     */
    public static String displayTime(ZonedDateTime zdt) {
        return displayTime(zdt, true);
    }

    /**
     * @param instant Object Instant.
     * @param zoneId  Object ZoneId.
     * @return date String in yyy-MM-dd HH:mm:ss
     */
    public static String displayTime(Instant instant, ZoneId zoneId) {
        return displayTime(getDate(instant, zoneId));
    }

    /**
     * @param zdt       Object ZonedDateTime.
     * @param useSecond Boolean to indicate to use second or not.
     * @return date String in yyy-MM-dd HH:mm:ss
     */
    public static String displayTime(ZonedDateTime zdt, boolean useSecond) {
        if (useSecond) {
            TIME_FORMATTER_1.format(zdt);
        }
        return TIME_FORMATTER_2.format(zdt);
    }

    /**
     * @param instant   Object Instant.
     * @param zoneId    Object ZoneId.
     * @param useSecond Boolean to indicate to use second or not.
     * @return date String in yyy-MM-dd HH:mm:ss
     */
    public static String displayTime(Instant instant, ZoneId zoneId, boolean useSecond) {
        return displayTime(getDate(instant, zoneId), useSecond);
    }

    public static String displayIso8601(ZonedDateTime zdt) {
        return ISO8601_FORMATTER.format(zdt);
    }

    public static String displayIso8601(Instant instant, ZoneId zoneId) {
        return displayIso8601(getDate(instant, zoneId));
    }

    public static String displayIso8601Lite(ZonedDateTime zdt) {
        return ISO8601_LITE_FORMATTER.format(zdt);
    }

    public static String displayIso8601Lite(Instant instant, ZoneId zoneId) {
        return displayIso8601Lite(getDate(instant, zoneId));
    }

    public static ZonedDateTime getStartOfDay(ZonedDateTime zdt) {
        return zdt.with(LocalTime.MIN);
    }

    public static ZonedDateTime getStartOfDay(Instant instant, ZoneId zoneId) {
        return getStartOfDay(getDate(instant, zoneId));
    }

    public static ZonedDateTime getEndOfDay(ZonedDateTime zdt) {
        return zdt.with(LocalTime.MAX);
    }

    public static ZonedDateTime getEndOfDay(Instant instant, ZoneId zoneId) {
        return getEndOfDay(getDate(instant, zoneId));
    }

    public static ZonedDateTime toUtc(ZonedDateTime zdt) {
        return zdt.withZoneSameInstant(ZoneId.of("UTC"));
    }

    public static void setDefaultZoneId(ZoneId zoneId) {
        DEFAULT_ZONE_ID = zoneId;
    }
}
