package co.lixionary.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class JsonUtil {

    private static final ObjectMapper CAMEL_CASE_MAPPER;
    private static final ObjectMapper SNAKE_CASE_MAPPER;

    static {
        CAMEL_CASE_MAPPER = createDefaultMapper();
        SNAKE_CASE_MAPPER = createDefaultMapper(PropertyNamingStrategy.SNAKE_CASE);
    }

    public static ObjectMapper getDefaultMapper() {
        return getDefaultSnakeCaseMapper();
    }

    public static ObjectMapper getDefaultCamelCaseMapper() {
        return CAMEL_CASE_MAPPER;
    }

    public static ObjectMapper getDefaultSnakeCaseMapper() {
        return SNAKE_CASE_MAPPER;
    }

    public static <T> List<T> fromJsonToList(String content, Class<T> elementClass) {
        return fromJsonToList(getDefaultMapper(), content, elementClass);
    }

    public static <T> List<T> fromJsonToList(ObjectMapper mapper, String content, Class<T> elementClass) {
        return fromJsonToCollection(mapper, content, List.class, elementClass);
    }

    public static <T extends Collection, V> T fromJsonToCollection(String content, Class<? extends Collection> collectionClass, Class<V> elementClass) {
        return fromJsonToCollection(getDefaultMapper(), content, collectionClass, elementClass);
    }

    public static <T extends Collection, V> T fromJsonToCollection(ObjectMapper mapper, String content, Class<? extends Collection> collectionClass, Class<V> elementClass) {
        try {
            return mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(collectionClass, elementClass));
        } catch (IOException ex) {
            throw new RuntimeException("Failed to convert JSON to Java Collection object.", ex);
        }
    }

    public static <T> T fromJsonSnakeCase(String content, Class<T> valueType) {
        return fromJson(getDefaultSnakeCaseMapper(), content, valueType);
    }

    public static <T> T fromJsonCamelCase(String content, Class<T> valueType) {
        return fromJson(getDefaultCamelCaseMapper(), content, valueType);
    }

    public static <T> T fromJson(String content, Class<T> valueType) {
        return fromJson(getDefaultMapper(), content, valueType);
    }

    public static <T> T fromJson(ObjectMapper mapper, String content, Class<T> valueType) {
        try {
            return mapper.readValue(content, valueType);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to convert JSON to Java object.", ex);
        }
    }

    public static String toJsonSnakeCase(Object value) {
        return toJson(getDefaultSnakeCaseMapper(), true, value, (String[]) null);
    }

    public static String toJsonCamelCase(Object value) {
        return toJson(getDefaultCamelCaseMapper(), true, value, (String[]) null);
    }

    public static String toJson(Object value) {
        return toJson(getDefaultMapper(), true, value, (String[]) null);
    }

    public static String toJson(ObjectMapper mapper, Object value) {
        return toJson(mapper, true, value, (String[]) null);
    }

    public static JsonNode toJsonNodeSnakeCase(Object value) {
        return SNAKE_CASE_MAPPER.valueToTree(value);
    }

    public static String toJson(ObjectMapper mapper, boolean pretty, Object value, String... selectedFields) {
        String result;
        ObjectWriter writer;

        if (selectedFields != null && selectedFields.length > 0) {
            writer = mapper.writer(new SimpleFilterProvider(Collections.singletonMap("myJsonFilter", SimpleBeanPropertyFilter.filterOutAllExcept(selectedFields))));
        } else {
            writer = mapper.writer();
        }

        if (pretty) {
            writer.withDefaultPrettyPrinter();
        }

        try {
            result = writer.writeValueAsString(value);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to convert Java object to String JSON.", ex);
        }

        return result;
    }

    private static ObjectMapper createDefaultMapper() {
        return createDefaultMapper(null);
    }

    private static ObjectMapper createDefaultMapper(PropertyNamingStrategy strategy) {
        ObjectMapper mapper = new ObjectMapper();

        if (strategy != null) {
            mapper.setPropertyNamingStrategy(strategy);
        }

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new ParameterNamesModule());
        mapper.registerModule(new JavaTimeModule());

        return mapper;
    }
}
