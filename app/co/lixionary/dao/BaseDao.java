package co.lixionary.dao;

import co.lixionary.context.DatabaseExecutionContext;
import play.db.jpa.JPAApi;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

public class BaseDao<T, ID> {

    protected final DatabaseExecutionContext executionContext;
    private final JPAApi jpaApi;
    private final Class<T> clazz;

    public BaseDao(Class<T> clazz, JPAApi jpaApi, DatabaseExecutionContext executionContext) {
        this.clazz = clazz;
        this.jpaApi = jpaApi;
        this.executionContext = executionContext;
    }

    public JPAApi getJpaApi() {
        return jpaApi;
    }

    public CompletionStage<T> create(T entity) {
        return CompletableFuture.supplyAsync(() ->
                        wrap(em -> insert(em, entity)),
                executionContext);
    }

    public CompletionStage<T> delete(T entity) {
        return CompletableFuture.supplyAsync(() ->
                        wrap(em -> remove(em, entity)),
                executionContext);
    }

    public CompletionStage<T> read(ID id) {
        return CompletableFuture.supplyAsync(() ->
                        wrap(em -> findById(em, id)),
                executionContext);
    }

    public CompletionStage<Stream<T>> read() {
        return CompletableFuture.supplyAsync(() ->
                        wrap(this::list),
                executionContext);
    }

    protected <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    protected String getEntityName() {
        return clazz.getSimpleName();
    }

    private T findById(EntityManager em, ID id) {
        return em.find(clazz, id);
    }

    private T insert(EntityManager em, T entity) {
        em.persist(entity);
        return entity;
    }

    private T remove(EntityManager em, T entity) {
        em.remove(em.contains(entity) ? entity : em.merge(entity));
        return entity;
    }

    private Stream<T> list(EntityManager em) {
        String sb = "select e from " +
                getEntityName() +
                " " +
                "e";
        List<T> results = em.createQuery(sb, clazz).getResultList();
        return results.stream();
    }
}