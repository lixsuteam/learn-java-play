package co.lixionary.dao;

import co.lixionary.context.DatabaseExecutionContext;
import co.lixionary.exception.NvNotFoundException;
import co.lixionary.model.Person;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class PersonDao extends BaseDao<Person, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDao.class.getName());

    @Inject
    public PersonDao(JPAApi jpaApi, DatabaseExecutionContext executionContext) {
        super(Person.class, jpaApi, executionContext);
    }

    public CompletionStage<Person> findByName(String name) {
        return CompletableFuture.supplyAsync(() -> {
            String query = new StringBuilder()
                    .append("SELECT p from ")
                    .append(getEntityName()).append(" p ")
                    .append("WHERE p.name = :name")
                    .toString();
            List<Person> people = wrap(em -> em
                    .createQuery(query, Person.class)
                    .setParameter("name", name)
                    .getResultList());
            if (people.isEmpty()) {
                throw new NvNotFoundException(String.format("people with name: %s not found", name));
            }
            if (people.size() > 1) {
                LOGGER.warn("result size: {} more than 1", people.size());
            }
            return people.get(0);
        }, executionContext);
    }
}
