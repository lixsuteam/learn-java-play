package co.lixionary.controllers.demo;

import co.lixionary.config.KafkaConstants;
import co.lixionary.controllers.BaseController;
import co.lixionary.manager.KafkaManager;
import co.lixionary.model.NvError;
import co.lixionary.model.ResultWrapper;
import com.google.inject.Inject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Result;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class KafkaController extends BaseController implements KafkaConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaController.class.getName());

    private final KafkaManager kafkaManager;

    @Inject
    public KafkaController(HttpExecutionContext executionContext, KafkaManager kafkaManager) {
        super(executionContext);
        this.kafkaManager = kafkaManager;
    }

    public CompletionStage<Result> publish(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {
            String message = request.getQueryString("message");
            String topic = TOPIC_DEMO_1;

            if (request.getQueryString("topic") != null) {
                topic = TOPIC_DEMO_2;
            }
            LOGGER.info("demo:kafka endpoint reached, message: {}", message);
            List<ProducerRecord<Long, String>> generatedTopics = new ArrayList<>();
            String str = "0";
            for (int i = 0; i < 100; i++) {
                str += i;
                String val = "This is record " + this.toString() + "_" + message + "_" + str;
                ProducerRecord<Long, String> record = new ProducerRecord<>(topic,
                        Instant.now().toEpochMilli(), val);
                generatedTopics.add(record);
            }
            generatedTopics.forEach(record -> {
                try {
                    RecordMetadata metadata = kafkaManager.getProducer().send(record).get();
                    LOGGER.info("[kafka producer] | offset: {} | partition: {} | value size: {} | value: {}", metadata.offset(), metadata.partition(), metadata.serializedValueSize(), record.value());
                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            });
            return "ok";
        }, executionContext.current())
                .thenApply((Function<String, ResultWrapper>) ResultWrapper::new)
                .exceptionally(throwable -> new ResultWrapper(new NvError(throwable)))
                .thenApply(this::processResult);
    }
}
