package co.lixionary.controllers;

import co.lixionary.model.NvError;
import co.lixionary.model.ResultWrapper;
import co.lixionary.util.JsonUtil;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.UUID;

public class BaseController extends Controller {
    private static final String NV_REQUEST_ID_HEADER = "x-nv-request-id";
    private static final String NV_SYSTEM_ID_HEADER = "x-nv-system-id";

    protected final HttpExecutionContext executionContext;

    public BaseController(HttpExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    protected String getRequestId(Http.Request request) {
        return request.header(NV_REQUEST_ID_HEADER).orElse(UUID.randomUUID().toString());
    }

    protected String getSystemId(Http.Request request) {
        return request.header(NV_SYSTEM_ID_HEADER).orElse("SG");
    }

    protected Result processResult(ResultWrapper resultWrapper) {
        if (resultWrapper.getError() == null) {
            return ok(JsonUtil.toJsonNodeSnakeCase(resultWrapper));
        }
        NvError error = resultWrapper.getError();
        switch (error.getCode()) {
            case NvError.ERR_BAD_REQUEST:
                return badRequest(JsonUtil.toJsonNodeSnakeCase(resultWrapper));
            case NvError.ERR_NOT_FOUND:
                return notFound(JsonUtil.toJsonNodeSnakeCase(resultWrapper));
            default:
                return internalServerError(JsonUtil.toJsonNodeSnakeCase(resultWrapper));
        }
    }
}
