package co.lixionary.controllers;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class HomeController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class.getName());

    @Inject
    public HomeController(HttpExecutionContext executionContext) {
        super(executionContext);
    }

    public CompletionStage<Result> home() {
        LOGGER.info("health check called, controller ref: " + this.toString());
        return CompletableFuture.supplyAsync(() -> ok("service ready\n"), executionContext.current());
    }
}
