package co.lixionary.controllers;

import co.lixionary.manager.PersonManager;
import co.lixionary.manager.factory.PersonManagerFactory;
import co.lixionary.model.NvError;
import co.lixionary.model.Person;
import co.lixionary.model.ResultWrapper;
import co.lixionary.util.JsonUtil;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Result;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class PersonController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class.getName());

    private final PersonManagerFactory personManagerFactory;

    @Inject
    public PersonController(PersonManagerFactory personManagerFactory, HttpExecutionContext executionContext) {
        super(executionContext);
        this.personManagerFactory = personManagerFactory;
    }

    public CompletionStage<Result> create(Http.Request request) {
        Person person = JsonUtil.fromJson(request.body().asJson().toString(), Person.class);
        return getPersonManager(request)
                .create(person)
                .thenApply((Function<Person, ResultWrapper>) ResultWrapper::new)
                .exceptionally(throwable -> new ResultWrapper(new NvError(throwable)))
                .thenApply(this::processResult);
    }

    public CompletionStage<Result> getAll(Http.Request request) {
        return getPersonManager(request)
                .getAll()
                .thenApply((Function<List<Person>, ResultWrapper>) ResultWrapper::new)
                .exceptionally(ResultWrapper::new)
                .thenApply(this::processResult);
    }

    public CompletionStage<Result> get(Http.Request request, String name) {
        if (name == null) {
            return CompletableFuture.supplyAsync(() ->
                    new ResultWrapper<>(new NvError("person name is mandatory", NvError.ERR_BAD_REQUEST)), executionContext.current()
            ).thenApply(this::processResult);
        }
        return getPersonManager(request).get(name)
                .thenApply((Function<Person, ResultWrapper>) ResultWrapper::new)
                .exceptionally(throwable -> new ResultWrapper(new NvError(throwable)))
                .thenApply(this::processResult);
    }

    private PersonManager getPersonManager(final Http.Request request) {
        return personManagerFactory.getPersonManager(getRequestId(request), getSystemId(request));
    }
}
