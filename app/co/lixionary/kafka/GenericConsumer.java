package co.lixionary.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenericConsumer<K, V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericConsumer.class.getName());

    private final ConsumerRecord<K, V> record;

    public GenericConsumer(ConsumerRecord<K, V> record) {
        this.record = record;
    }

    public void run() {
        LOGGER.info("[kafka consumer] thread id: @{} | topic: {} | offset: {} | partition: {} | key size: {} | value size {}",
                Thread.currentThread().getId(), record.topic(), record.offset(), record.partition(), record.key(), record.value());
    }
}
