package co.lixionary.model;

import co.lixionary.exception.NvNotFoundException;

public class NvError {

    public static final int ERR_NOT_FOUND = 404;
    public static final int ERR_BAD_REQUEST = 400;
    public static final int ERR_SERVER_ERROR = 500;

    private String message;
    private int code;
    private String[] stackTraces;

    public NvError(String message, int code, String[] stackTraces) {
        this.message = message;
        this.code = code;
        this.stackTraces = stackTraces;
    }

    public NvError(String message) {
        this.message = message;
        this.code = 500;
    }

    public NvError(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public NvError(Throwable ex) {
        if (ex instanceof NvNotFoundException) {
            this.message = ex.getMessage();
            this.code = 404;
        } else {
            this.message = ex.getMessage();
            this.code = 500;
            StackTraceElement[] stackTraceElements = ex.getStackTrace();
            if (stackTraceElements != null && stackTraceElements.length > 0) {
                this.stackTraces = new String[stackTraceElements.length];
                for (int i = 0; i < stackTraceElements.length; i++) {
                    stackTraces[i] = stackTraceElements[i].toString();
                }
            }
        }
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public String[] getStackTraces() {
        return stackTraces;
    }
}
