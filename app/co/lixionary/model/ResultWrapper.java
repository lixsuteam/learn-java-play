package co.lixionary.model;

public class ResultWrapper<T> {

    private T data;
    private NvError error;

    public ResultWrapper(T data) {
        this.data = data;
    }

    public ResultWrapper(T data, NvError error) {
        this.data = data;
        this.error = error;
    }

    public ResultWrapper(NvError error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public NvError getError() {
        return error;
    }

    public void setError(NvError error) {
        this.error = error;
    }
}
