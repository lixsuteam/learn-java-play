package co.lixionary.client;

import co.lixionary.config.NvEnv;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NvMongoClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(NvMongoClient.class.getName());

    private final MongoClient mongoClient;

    public NvMongoClient() {
        final String host = NvEnv.NV_MONGO_DB_HOST.getValue();
        final String username = NvEnv.NV_MONGO_DB_USERNAME.getValue();
        final String password = NvEnv.NV_MONGO_DB_PASSWORD.getValue();
        final String dbName = NvEnv.NV_MONGO_DB_NAME.getValue();
        final int port = Integer.valueOf(NvEnv.NV_MONGO_DB_PORT.getValue());

        ServerAddress serverAddress = new ServerAddress(host, port);
        MongoCredential mongoCredential = MongoCredential
                .createCredential(username, dbName, password.toCharArray());
        MongoClientOptions mongoClientOptions = MongoClientOptions.builder()
                .minConnectionsPerHost(23)
                .build();

        this.mongoClient = new MongoClient(serverAddress, mongoCredential, mongoClientOptions);
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public MongoDatabase getMongoDatabase() {
        if (mongoClient != null) {
            final String dbName = NvEnv.NV_MONGO_DB_NAME.getValue();
            return mongoClient.getDatabase(dbName);
        }
        throw new RuntimeException("mongo client instance is null");
    }
}
