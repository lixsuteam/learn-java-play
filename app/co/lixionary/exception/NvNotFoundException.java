package co.lixionary.exception;

public class NvNotFoundException extends RuntimeException {

    public NvNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NvNotFoundException(String message) {
        super(message);
    }
}
