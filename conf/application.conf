# This is the main configuration file for the application.
# https://www.playframework.com/documentation/latest/ConfigFile

play {
  server {
    provider = "play.core.server.AkkaHttpServerProvider"
    akka {
      bindTimeout = 5 seconds
      requestTimeout = infinite
      transparent-head-requests = off
      default-host-header = ""
      server-header = null
      server-header = ${?play.server.server-header}
      illegal-response-header-value-processing-mode = warn
      max-content-length = infinite
      max-header-value-length = 8k
      tls-session-info-header = on
    }
  }
}
play.http.filters = play.api.http.NoHttpFilters
play.filters.enabled += co.lixionary.filter.SimpleFilters
play.modules.enabled += co.lixionary.guice.Module

jpa.default = defaultPersistenceUnit
db {
  default.jndiName = DefaultDS
  default.driver = "com.mysql.jdbc.Driver"
  default.url = "jdbc:mysql://127.0.0.1:9306/learn-java-play?characterEncoding=UTF-8"
  default.password = "password"
}

# db connections = ((physical_core_count * 2) + effective_spindle_count)
fixedDbConnectionPool = 20
fixedKafkaRunnerPool = 1
fixedKafkaConsumerPool = 5
fixedWorkerPool = 10
fixedSlowOperationPool = 2

akka {
  actor {
    default-dispatcher {
      executor = "thread-pool-executor"
      throughput = 2
      thread-pool-executor {
        fixed-pool-size = 43 # + number of cores (2) + housekeeping (1)
      }
    }
  }
}

database.dispatcher {
  executor = "thread-pool-executor"
  throughput = 1
  thread-pool-executor {
    fixed-pool-size = ${fixedDbConnectionPool}
  }
}

worker.dispatcher {
  executor = "thread-pool-executor"
  throughput = ${fixedWorkerPool}
  thread-pool-executor {
    fixed-pool-size = ${fixedWorkerPool}
  }
}

kafka-runner.dispatcher {
  executor = "thread-pool-executor"
  throughput = ${fixedKafkaRunnerPool}
  thread-pool-executor {
    fixed-pool-size = ${fixedKafkaRunnerPool}
  }
}

kafka-consumer.dispatcher {
  executor = "thread-pool-executor"
  throughput = ${fixedKafkaConsumerPool}
  thread-pool-executor {
    fixed-pool-size = ${fixedKafkaConsumerPool}
  }
}

slowWorker.dispatcher {
  executor = "thread-pool-executor"
  throughput = 1
  thread-pool-executor {
    fixed-pool-size = ${fixedSlowOperationPool}
  }
}