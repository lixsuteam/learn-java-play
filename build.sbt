name := """learn_play"""
organization := "com.lixionary"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(javaJpa, cacheApi, javaWs, guice,
  "com.google.inject" % "guice" % "4.0",
  "com.fasterxml.jackson.datatype" % "jackson-datatype-jdk8" % "2.9.8",
  "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % "2.9.8",
  "com.fasterxml.jackson.module" % "jackson-module-parameter-names" % "2.9.8",
  "org.apache.kafka" % "kafka-clients" % "2.2.0",
  "com.sendgrid" % "sendgrid-java" % "4.3.0",
  "org.mongodb" % "mongo-java-driver" % "3.10.2",
  "org.hibernate" % "hibernate-entitymanager" % "5.4.2.Final",
  "mysql" % "mysql-connector-java" % "5.1.47",
)

PlayKeys.externalizeResourcesExcludes += baseDirectory.value / "conf" / "META-INF" / "persistence.xml"
